$("button").on("click", function(){

    var id_do_professor = $("input").val();
    var url_da_api = "https://icuff17-api.herokuapp.com/teachers"
    var url_de_um_prof = url_da_api + '/' + id_do_professor; 
    
    /*$.ajax({
        url: url_de_um_prof
    }).done(function(data){
        alert(data.name);  //alert(data[0]["name"]);
    }).fail(function(data){
        alert("Não encontrado professor com id " + id_do_professor);
    });

    OU
    */

    var requisicao_professor = $.ajax({url: url_de_um_prof});

    requisicao_professor.done(function(data){ 

        var nome_professor = data["name"] + ' (id = ' +id_do_professor + ') '
        var foto_professor = "<img src = " + data["photo"] + " >"
        var infos_professor = $("<li></li>").text(nome_professor).append(foto_professor);

        $("#professores_encontrados").append(infos_professor);
        });

    requisicao_professor.fail(function(data){ 
        alert("Não encontrado professor com id " + id_do_professor);
    });

})